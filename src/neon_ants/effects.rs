use std::time::Duration;
use bevy::prelude::*;

pub const DEFAULT_EFFECT_DURATION: Duration = Duration::from_millis(500);

pub struct EffectsPlugin;
impl Plugin for EffectsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(Update, effect_timeout_system);
	}
}

#[derive(Component, Copy, Clone)]
pub struct TempEffect
{
	pub elapsed: Duration,
	pub timeout: Duration,
}
impl Default for TempEffect
{
	fn default() -> Self
	{
		TempEffect
		{
			elapsed: default(),
			timeout: DEFAULT_EFFECT_DURATION,
		}
	}
}

#[derive(Bundle)]
pub struct TempEffectBundle<T: Bundle>
{
	pub display_bundle: T,
	pub effect: TempEffect,
	pub name: Name,
}
impl<T: Bundle> TempEffectBundle<T>
{
	pub fn new(name: impl Into<Name>, display_bundle: T, duration: Option<Duration>) -> Self
	{
		Self
		{
			display_bundle,
			effect: TempEffect
			{
				timeout: duration.unwrap_or(DEFAULT_EFFECT_DURATION),
				..default()
			},
			name: name.into(),
		}
	}
}

pub fn effect_timeout_system
(
	mut commands: Commands,
	mut effects: Query
	<(
		Entity,
		&mut TempEffect
	)>,
	time: Res<Time>,
)
{
	for (entity, mut effect) in effects.iter_mut()
	{
		effect.elapsed += time.delta();
		if effect.elapsed >= effect.timeout
		{
			commands.entity(entity).despawn_recursive();
		}
	}
}