use bevy::prelude::*;
use crate::*;

pub const ANT_SOLDIER_FIND_RADIUS: f32 = ANT_SCOUT_FIND_RADIUS;
pub const ANT_SOLDIER_REPORT_NONE_RADIUS: f32 = ANT_WORKER_REPORT_NONE_RADIUS;
pub const ANT_SOLDIER_SPEED: f32 = ANT_BASE_SPEED * 0.8;
pub const ANT_SOLDIER_DAMAGE: f32 = ANT_HEALTH;

pub struct AntSoldierPlugin;
impl Plugin for AntSoldierPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<SoldierFightTarget>();
		app.add_systems(FixedUpdate,
		(
			soldier_receive_reports_system,
			soldier_find_enemies_system,
			soldier_attack_enemies_system,
		));
	}
}

#[derive(Component, Default)]
pub struct SoldierClass
{
}

#[derive(Component, Reflect)]
pub struct SoldierFightTarget(pub Vec2);

pub fn soldier_receive_reports_system
(
	mut commands: Commands,
	soldiers: Query
	<
		(
			Entity,
			&AntColonyAllegiance,
			&RigidBody,
		),
		(
			With<SoldierClass>,
			Without<News>,
			Without<WorkerHarvestTarget>,
		),
	>,
	mut reports: EventReader<NewsDistributeEvent>,
)
{
	for report in reports.read()
	{
		if report.news.news_type == NewsType::Enemy
		{
			for (entity, allegiance, rigid_body) in soldiers.iter()
			{
				if *rigid_body == RigidBody::KinematicVelocityBased && allegiance.colony == report.colony
				{
					commands.entity(entity).insert(SoldierFightTarget(report.news.location));
					break;
				}
			}
		}
	}
}

pub fn soldier_find_enemies_system
(
	mut commands: Commands,
	mut soldiers: Query
	<
		(
			Entity,
			&mut Velocity,
			&Transform,
			&SoldierFightTarget,
			&AntColonyAllegiance,
		),
		Without<News>,
	>,
	enemies: Query
	<
		(
			Entity,
			&Transform,
			Option<&AntColonyAllegiance>,
		),
		With<Health>,
	>,
)
{
	for (entity, mut velocity, soldier_transform, target, allegiance) in soldiers.iter_mut()
	{
		let soldier_xy = soldier_transform.translation.xy();
		let mut target_pos = target.0;
		let nearest_enemy = enemies
			.iter()
			.filter(|(enemy, _, enemy_allegiance)|
			{
				if let Some(enemy_allegiance) = enemy_allegiance
				{
					enemy_allegiance.colony != allegiance.colony
				}
				else
				{
					*enemy != allegiance.colony
				}
			})
			.map(|(_, transform, _)|
			(
				transform,
				transform.translation.xy().distance_squared(soldier_xy),
			))
			.filter(|(_, dist2)| *dist2 <= ANT_SOLDIER_FIND_RADIUS * ANT_SOLDIER_FIND_RADIUS)
			.min_by(|(_, dist_a_2), (_, dist_b_2)| dist_a_2.total_cmp(&dist_b_2))
			.map(|(transform, _)| transform);
		if let Some(nearest_enemy_transform) = nearest_enemy
		{
			target_pos = nearest_enemy_transform.translation.xy();
		}
		let delta = target_pos - soldier_xy;
		velocity.linvel = delta.normalize_or_zero() * ANT_SOLDIER_SPEED;
		let dist2 = delta.length_squared();
		if
			nearest_enemy.is_none()
			&& dist2 <= ANT_SOLDIER_REPORT_NONE_RADIUS * ANT_SOLDIER_REPORT_NONE_RADIUS
		{
			commands.entity(entity)
				.insert(News
				{
					location: target.0,
					news_type: NewsType::Enemy,
					not_found: true,
				})
				.remove::<SoldierFightTarget>();
		}
	};
}

pub fn soldier_attack_enemies_system
(
	mut targets: Query
	<
		(
			Entity,
			&mut Health,
			Option<&SoldierClass>,
		),
		With<SoldierClass>,
	>,
	rapier_context: Res<RapierContext>,
	time: Res<Time>,
)
{
	let tick_damage = ANT_SOLDIER_DAMAGE * time.delta_seconds();
	let mut combinations = targets.iter_combinations_mut::<2>();
	while let Some
	([
		(entity_a, mut health_a, soldier_a),
		(entity_b, mut health_b, soldier_b),
	]) = combinations.fetch_next()
	{
		let damage_a = soldier_b.is_some();
		let damage_b = soldier_a.is_some();
		if damage_a || damage_b
		{
			if let Some(pair) = rapier_context.contact_pair(entity_a, entity_b)
			{
				if pair.has_any_active_contact()
				{
					if damage_a {health_a.current -= tick_damage;}
					if damage_b {health_b.current -= tick_damage;}
				}
			}
		}
	}
}