pub mod neon_ants;
pub mod debug_gui;
use bevy::prelude::*;
use bevy::asset::*;
use bevy_rapier2d::prelude::*;
use bevy_svg::prelude::*;
use neon_ants::*;
use debug_gui::*;

fn main()
{
	let mut app = App::new();
    app.add_plugins
	((
		DefaultPlugins.build()
			.set(WindowPlugin
			{
				primary_window: Some(Window
				{
					canvas: Some("#canvas".into()),
					fit_canvas_to_parent: true,
					..default()
				}),
				..default()
			})
			.set(AssetPlugin
			{
				meta_check: AssetMetaCheck::Never,
				..default()
			}),
		RapierPhysicsPlugin::<NoUserData>::pixels_per_meter(1.0),
		SvgPlugin,
		NeonAntsPlugin,
		DebugGUIPlugin,
	));
	app.run();
}