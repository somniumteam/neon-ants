use std::time::Duration;
use std::f32::consts::PI;
use bevy::prelude::*;
use rand::{Rng, thread_rng};
use crate::*;

pub const ANT_SCOUT_WANDER_DURATION_MAX: Duration = Duration::from_secs(3);
pub const ANT_SCOUT_WANDER_SPEED: f32 = ANT_SCOUT_SPEED * 1.5;
pub const ANT_SCOUT_SPEED: f32 = ANT_BASE_SPEED;
pub const ANT_SCOUT_REPORT_RADIUS: f32 = ANT_HEIGHT/ 2.0;
pub const ANT_SCOUT_FIND_RADIUS: f32 = ANT_HEIGHT * 8.0;
pub const ANT_SCOUT_ALERT_EFFECT_SCALE: f32 = 0.4;
pub const ANT_SCOUT_ALERT_JITTER_WIDTH: f32 = 8.0;

pub struct AntScoutPlugin;
impl Plugin for AntScoutPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<ScoutClass>();
		app.add_systems(Startup, setup);
		app.add_systems(FixedUpdate,
		(
			scout_wander_system,
			scout_find_food_system,
			scout_find_enemy_system,
		));
	}
}
fn setup
(
	mut commands: Commands,
	assets: Res<AssetServer>,
)
{
	commands.insert_resource(AntScoutSingletons
	{
		alert_effect_image: assets.load("scout-alert.svg").into(),
	});
}

#[derive(Resource)]
pub struct AntScoutSingletons
{
	pub alert_effect_image: Handle<Svg>,
}

#[derive(Component, Default, Reflect)]
pub struct ScoutClass
{
	pub wander_elapsed: Duration,
	pub wander_target_elapsed: Duration,
}

pub fn scout_wander_system
(
	time: Res<Time>,
	mut ants: Query
	<
		(
			&mut ScoutClass,
			&mut Velocity,
		),
		Without<News>,
	>,
)
{
	let mut rand = rand::thread_rng();
	for (mut scout, mut velocity) in ants.iter_mut()
	{
		scout.wander_elapsed += time.delta();
		if scout.wander_elapsed > scout.wander_target_elapsed
		{
			scout.wander_elapsed = Duration::ZERO;
			let max_millis = ANT_SCOUT_WANDER_DURATION_MAX.as_millis() as u64;
			let target_millis = rand.gen_range(0..max_millis);
			scout.wander_target_elapsed = Duration::from_millis(target_millis);
			velocity.linvel = Vec2::from_angle(rand.gen_range(0.0..(PI * 2.0))) * ANT_SCOUT_WANDER_SPEED;
		}
	}
}

pub fn scout_find_food_system
(
	commands: ParallelCommands,
	scouts: Query
	<
		(
			Entity,
			&Transform,
		),
		(
			With<ScoutClass>,
			Without<News>,
		),
	>,
	food: Query
	<
		&Transform,
		(
			With<IsPlant>,
			With<Health>,
		),
	>,
	singletons: Res<AntScoutSingletons>,
)
{
	food.par_iter().for_each(|food_transform|
	{
		let food_location = food_transform.translation.xy();
		for (scout, transform) in scouts.iter()
		{
			let dist2 = transform.translation.xy().distance_squared(food_location);
			if dist2 <= ANT_SCOUT_FIND_RADIUS * ANT_SCOUT_FIND_RADIUS
			{
				commands.command_scope(|mut commands|
				{
					commands.entity(scout).insert(News
					{
						news_type: NewsType::Food,
						location: food_location,
						not_found: false,
					});
					let mut rand = thread_rng();
					let alert_location = food_location
						+ Vec2::new(rand.gen_range(-0.5..0.5), rand.gen_range(-0.5..0.5))
						* ANT_SCOUT_ALERT_JITTER_WIDTH;
					commands.spawn(scout_alert_effect_bundle(alert_location, &singletons));
				});
			}
		}
	});
}

pub fn scout_find_enemy_system
(
	commands: ParallelCommands,
	scouts: Query
	<
		(
			Entity,
			&Transform,
			&AntColonyAllegiance,
		),
		(
			With<ScoutClass>,
			Without<News>,
		),
	>,
	enemies: Query
	<
		(
			Entity,
			&Transform,
			Option<&AntColonyAllegiance>
		),
		(
			With<Health>,
			Without<IsPlant>,
		),
	>,
)
{
	enemies.par_iter().for_each(|(enemy, enemy_transform, enemy_allegiance)|
	{
		let enemy_location = enemy_transform.translation.xy();
		for (scout, transform, allegiance) in scouts.iter()
		{
			if enemy == allegiance.colony {continue;}
			else if let Some(enemy_allegiance) = enemy_allegiance
			{
				if allegiance.colony == enemy_allegiance.colony {continue;}
			}
			let dist2 = transform.translation.xy().distance_squared(enemy_location);
			if dist2 <= ANT_SCOUT_FIND_RADIUS * ANT_SCOUT_FIND_RADIUS
			{
				commands.command_scope(|mut commands|
				{
					commands.entity(scout).insert(News
					{
						news_type: NewsType::Enemy,
						location: enemy_location,
						not_found: false,
					});
				});
			}
		}
	});
}

pub fn scout_alert_effect_bundle(position: Vec2, singletons: &AntScoutSingletons) -> TempEffectBundle<Svg2dBundle>
{
	TempEffectBundle::new
	(
		"Scout Alert",
		Svg2dBundle
		{
			svg: singletons.alert_effect_image.clone(),
			origin: Origin::Center,
			transform: Transform
			{
				translation: position.extend(1.0),
				scale: Vec3::splat(ANT_SCOUT_ALERT_EFFECT_SCALE),
				..default()
			},
			..default()
		},
		None
	)
}