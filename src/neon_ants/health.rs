use bevy::prelude::*;

pub struct HealthPlugin;
impl Plugin for HealthPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(FixedUpdate,
		(
			entity_death_system,
			entity_maximum_health_system,
		));
	}
}

#[derive(Component, Reflect)]
pub struct Health
{
	pub current: f32,
	pub maximum: f32,
}
impl Health
{
	pub fn new(maximum: f32) -> Self
	{
		Self
		{
			current: maximum,
			maximum,
		}
	}
}

#[derive(Component)]
pub struct IsDead;

#[derive(Component)]
pub struct RemoveOnDeath;

pub fn entity_death_system
(
	mut commands: Commands,
	query: Query
	<
		(
			Entity,
			&Health,
			Option<&RemoveOnDeath>,
		),
		Without<IsDead>,
	>,
)
{
	for (entity, health, remove) in query.iter()
	{
		if health.current <= 0.0
		{
			let mut commands = commands.entity(entity);
			if remove.is_some() {commands.despawn_recursive();}
			else {commands.remove::<Health>().insert(IsDead);}
		}
	}
}

pub fn entity_maximum_health_system(mut query: Query<&mut Health>)
{
	for mut health in query.iter_mut()
	{
		if health.current > health.maximum {health.current = health.maximum;}
	}
}