use bevy::prelude::*;
use crate::*;

pub struct CommunicationPlugin;
impl Plugin for CommunicationPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<News>();
		app.register_type::<SavedReports>();
		app.add_systems(PreUpdate,
		(
			news_alert_add_indicator_system,
			news_alert_remove_indicator_system,
		));
		app.add_systems(FixedUpdate,
		(
			colony_receive_reports_system,
			colony_distribute_reports_system,
			ant_report_news_system,
		));
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Reflect)]
pub enum NewsType
{
	Food,
	Enemy,
}

#[derive(Component, Reflect, Clone, Copy)]
pub struct News
{
	pub news_type: NewsType,
	pub location: Vec2,
	pub not_found: bool,
}
#[derive(Event)]
pub struct NewsReportEvent
{
	pub colony: Entity,
	pub news: News,
}
#[derive(Event)]
pub struct NewsDistributeEvent
{
	pub colony: Entity,
	pub news: News,
}
#[derive(Component, Default, Reflect)]
pub struct SavedReports(Vec<News>);

pub fn colony_receive_reports_system
(
	mut query: Query<&mut SavedReports>,
	mut reports: EventReader<NewsReportEvent>,
)
{
	for new_report in reports.read()
	{
		let proximity_closure = |prev_report: &News|
		{
			let new_report = new_report.news;
			prev_report.news_type == new_report.news_type
			&& (prev_report.location.distance_squared(new_report.location) <= ANT_COLONY_REPORT_PRUNE_RADIUS)
		};
		if let Ok(mut reports) = query.get_mut(new_report.colony)
		{
			let new_report = new_report.news;
			match new_report.not_found
			{
				false =>
				{
					if !reports.0.iter().any(proximity_closure){reports.0.push(new_report);}
				},
				true =>
				{
					reports.0.retain(|params| !proximity_closure(params));
				},
			}
		}
	}
}

pub fn colony_distribute_reports_system
(
	query: Query
	<(
		Entity,
		&SavedReports,
	)>,
	mut events: EventWriter<NewsDistributeEvent>,
)
{
	for (entity, reports) in query.iter()
	{
		for news_type in [NewsType::Food, NewsType::Enemy]
		{
			if let Some(news) = reports.0.iter().find(|news| news.news_type == news_type)
			{
				events.send(NewsDistributeEvent
				{
					colony: entity,
					news: *news,
				});
			}
		}
	}
}

#[derive(Component, Default)]
pub struct IsNewsAlert;
#[derive(Bundle, Default)]
pub struct NewsAlertBundle
{
	pub name: Name,
	pub svg: Svg2dBundle,
	pub is_news_alert: IsNewsAlert,
}
impl NewsAlertBundle
{
	pub fn new(singletons: &AntSingletons) -> Self
	{
		Self
		{
			name: Name::new("News Alert"),
			svg: Svg2dBundle
			{
				svg: singletons.news_alert_sprite.clone(),
				transform: singletons.news_alert_sprite_offset,
				..default()
			},
			..default()
		}
	}
}

pub fn news_alert_add_indicator_system
(
	mut commands: Commands,
	ants: Query
	<
		Entity,
		Added<News>,
	>,
	singletons: Res<AntSingletons>,
)
{
	for entity in ants.iter()
	{
		commands.entity(entity).with_children(|builder|
		{
			builder.spawn(NewsAlertBundle::new(&singletons));
		});
	}
}

pub fn news_alert_remove_indicator_system
(
	mut commands: Commands,
	ants: Query
	<
		&Children,
		Without<News>,
	>,
	alerts: Query
	<
		Entity,
		With<IsNewsAlert>,
	>,
)
{
	for children in ants.iter()
	{
		for child in children.iter()
		{
			if alerts.contains(*child)
			{
				commands.entity(*child).despawn_recursive();
			}
		}
	}
}

pub fn ant_report_news_system
(
	mut commands: Commands,
	colonies: Query
	<
		&Transform,
		With<AntColony>,
	>,
	mut ants: Query
	<(
		Entity,
		&mut Velocity,
		&Transform,
		&News,
		&AntColonyAllegiance,
	)>,
	mut report_events: EventWriter<NewsReportEvent>,
)
{
	for
	(
		entity,
		mut velocity,
		transform,
		news,
		allegiance,
	) in ants.iter_mut()
	{
		if let Ok(colony) = colonies.get(allegiance.colony)
		{
			let colony_transform = colony;
			let colony_position = colony_transform.translation.xy();
			let ant_position = transform.translation.xy();
			let delta = colony_position - ant_position;
			if delta.length_squared() <= ANT_SCOUT_REPORT_RADIUS
			{
				report_events.send(NewsReportEvent
				{
					colony: allegiance.colony,
					news: *news,
				});
				commands.entity(entity).remove::<News>();
			}
			else
			{
				velocity.linvel = delta.normalize_or_zero() * ANT_SCOUT_SPEED;
			}
		}
	}
}