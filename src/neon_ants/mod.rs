pub mod ants;
pub mod plants;
pub mod field_boundaries;
pub mod health;
pub mod effects;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
pub use ants::*;
pub use plants::*;
pub use field_boundaries::*;
pub use health::*;
pub use effects::*;

pub const BACKGROUND_COLOR: Color = Color::srgb(0.8, 0.8, 0.8);

pub struct NeonAntsPlugin;
impl Plugin for NeonAntsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<Health>();
		app.add_event::<GameRestartEvent>();
		app.insert_resource(RapierConfiguration
		{
			gravity: Vec2::default(),
			..RapierConfiguration::new(1.0)
		});
		app.insert_resource(ClearColor(BACKGROUND_COLOR));
		app.insert_resource(Time::<Fixed>::from_hz(8.0));
		app.add_plugins(FieldBoundariesPlugin);
		app.add_plugins(HealthPlugin);
		app.add_plugins(AntsPlugin);
		app.add_plugins(PlantsPlugin);
		app.add_plugins(EffectsPlugin);
		app.add_systems(Startup, setup);
	}
}
fn setup
(
	mut commands: Commands,
	mut restarts: EventWriter<GameRestartEvent>,
)
{
	commands.spawn(Camera2dBundle::default());
	restarts.send(GameRestartEvent);
}

#[derive(Event)]
pub struct GameRestartEvent;