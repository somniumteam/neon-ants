pub mod communication;
pub mod colony;
pub mod scout;
pub mod worker;
pub mod soldier;
use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
pub use communication::*;
pub use colony::*;
pub use scout::*;
pub use worker::*;
pub use soldier::*;
use std::fmt::Display;
use crate::*;

pub const ANT_HEIGHT: f32 = 16.0;
pub const ANT_WIDTH: f32 = ANT_HEIGHT * 0.75;
pub const ANT_COLLIDER_SCALE: f32 = 0.7;
pub const ANT_TRAFFIC_JITTER_STRENGTH: f32 = 16.0;
pub const ANT_FACING_MINIMUM_SPEED: f32 = 10.0;
pub const ANT_FACING_ROTATION_SPEED: f32 = std::f32::consts::PI * 2.0;
pub const ANT_BASE_SPEED: f32 = ANT_HEIGHT * 2.0;
pub const ANT_HEALTH: f32 = 10.0;
pub const ANT_CORPSE_COLOR: Color = Color::srgb(0.6, 0.6, 0.6);

pub const ANT_CLASS_COUNT: usize = 3;
#[derive(Default, Copy, Clone, Eq, PartialEq, Debug, Reflect, Component)]
#[repr(usize)]
pub enum AntClass
{
	#[default]
	SCOUT = 0,
	WORKER = 1,
	SOLDIER = 2,
}
impl Display for AntClass
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		let str = match *self
		{
			AntClass::SCOUT => "SCOUT",
			AntClass::WORKER => "WORKER",
			AntClass::SOLDIER => "SOLDIER",
		};
		write!(f, "{}", str)
	}
}
pub const ANT_CLASSES: [AntClass; ANT_CLASS_COUNT] = [AntClass::SCOUT, AntClass::WORKER, AntClass::SOLDIER];
pub const ANT_CLASS_NAMES: [&str; ANT_CLASS_COUNT] = ["SCOUT", "WORKER", "SOLDIER"];
pub const ANT_CLASS_MINIMUMS: [usize; ANT_CLASS_COUNT] = [1, 2, 0];
pub const ANT_CLASS_RATIOS: [f32; ANT_CLASS_COUNT] = [0.6, 0.3, 0.1];

pub struct AntsPlugin;
impl Plugin for AntsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<NewsType>();
		app.register_type::<News>();
		app.register_type::<AntClass>();
		app.add_event::<NewsReportEvent>();
		app.add_event::<NewsDistributeEvent>();
		app.add_plugins(CommunicationPlugin);
		app.add_plugins(AntColonyPlugin);
		app.add_plugins(AntScoutPlugin);
		app.add_plugins(AntWorkerPlugin);
		app.add_plugins(AntSoldierPlugin);
		app.add_systems(Startup, setup);
		app.add_systems(FixedUpdate,
		(
			ant_collider_state_system,
			ant_traffic_jitter_system,
			ant_death_system,
		));
		app.add_systems(Update, ant_facing_system);
	}
}
fn setup
(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
)
{
	let alert_size = Vec2::new(26.0, 18.0);
	let alert_scale_factor = 0.5 * ANT_HEIGHT / alert_size.y;
	let alert_size_scaled = alert_size * alert_scale_factor;
	commands.insert_resource(AntSingletons
	{
		ant_image: asset_server.load("AntBase.png"),
		news_alert_sprite: asset_server.load("comment-dots-solid.svg"),
		news_alert_sprite_offset: Transform
		{
			translation:
				Origin::Center.compute_translation(alert_size_scaled)
				+ alert_size_scaled.extend(0.1) * 0.5
				+ Vec3::new(ANT_WIDTH * 0.2, 0.0, 0.0),
			scale: Vec3::splat(alert_scale_factor),
			rotation: default(),
		},
	});
}

#[derive(Bundle)]
pub struct AntBundle
{
	pub name: Name,
	pub sprite: SpriteBundle,
	pub allegiance: AntColonyAllegiance,
	pub class: AntClass,
	pub health: Health,
	pub collider: Collider,
	pub rigid_body: RigidBody,
	pub active_events: ActiveEvents,
	pub damping: Damping,
	pub velocity: Velocity,
}
#[derive(Resource, Clone)]
pub struct AntSingletons
{
	pub ant_image: Handle<Image>,
	pub news_alert_sprite: Handle<Svg>,
	pub news_alert_sprite_offset: Transform,
}
impl AntBundle
{
	pub fn new
	(
		singletons: &AntSingletons,
		allegiance: AntColonyAllegiance,
		class: AntClass,
	) -> Self
	{
		Self
		{
			name: Name::new(format!("Ant {} {}", class, allegiance)),
			sprite: SpriteBundle
			{
				sprite: Sprite
				{
					color: allegiance.color,
					custom_size: Some(Vec2::new(ANT_WIDTH, ANT_HEIGHT)),
					..default()
				},
				texture: singletons.ant_image.clone(),
				..default()
			},
			allegiance,
			class,
			health: Health::new(ANT_HEALTH),
			collider: Collider::capsule_y
			(
				ANT_COLLIDER_SCALE * (ANT_HEIGHT / 2.0 - ANT_WIDTH),
				ANT_COLLIDER_SCALE * (ANT_WIDTH / 2.0)
			),
			active_events: ActiveEvents::COLLISION_EVENTS,
			rigid_body: RigidBody::KinematicVelocityBased,
			damping: Damping
			{
				angular_damping: f32::INFINITY,
				linear_damping: 0.0,
			},
			velocity: default(),
		}
	}
}

pub fn ant_collider_state_system
(
	mut ants: Query
	<(
		&AntColonyAllegiance,
		&Transform,
		&mut RigidBody,
	)>,
	colonies: Query
	<
		&Transform,
		With<AntColony>,
	>,
)
{
	for (allegiance, transform, mut rigid_body) in ants.iter_mut()
	{
		if let Ok(colony_transform) = colonies.get(allegiance.colony)
		{
			let target_rigid_body = 
				transform.translation.xy().distance_squared(colony_transform.translation.xy())
				<= ANT_COLONY_KINEMATIC_ANTS_RADIUS * ANT_COLONY_KINEMATIC_ANTS_RADIUS;
			let target_rigid_body = if target_rigid_body {RigidBody::KinematicVelocityBased} else {RigidBody::Dynamic};
			if target_rigid_body != *rigid_body {*rigid_body = target_rigid_body;}
		}
	}
}

pub fn ant_traffic_jitter_system
(
	mut ants: Query
	<
		(
			Entity,
			&mut Velocity,
		),
		With<AntClass>,
	>,
	rapier_context: Res<RapierContext>,
)
{
	let mut combinations = ants.iter_combinations_mut::<2>();
	while let Some
	([
		(entity_a, mut velocity_a),
		(entity_b, mut velocity_b)
	]) = combinations.fetch_next()
	{
		if let Some(collision) = rapier_context.contact_pair(entity_a, entity_b)
		{
			if collision.has_any_active_contact()
			{
				let manifold = collision.manifold(0).unwrap();
				let n1 = manifold.local_n1();
				let force_a = Vec2::new(n1.y, -n1.x) * ANT_TRAFFIC_JITTER_STRENGTH;
				let force_b = force_a * -1.0;
				velocity_a.linvel += force_a;
				velocity_b.linvel += force_b;
			}
		}
	}
}

pub fn ant_facing_system
(
	mut ants: Query
	<
		(
			&mut Transform,
			&mut Velocity,
		),
		With<AntClass>,
	>,
	time: Res<Time>,
)
{
	for (mut transform, mut velocity) in ants.iter_mut()
	{
		if velocity.angvel != 0.0 {velocity.angvel = 0.0;}
		if velocity.linvel.length_squared() >= ANT_FACING_MINIMUM_SPEED * ANT_FACING_MINIMUM_SPEED
		{
			let rotation = Quat::from_rotation_arc_2d(Vec2::Y, velocity.linvel.normalize());
			if transform.rotation != rotation
			{
				let angle_between = transform.rotation.angle_between(rotation);
				if angle_between > 0.0
				{
					let interpolation = time.delta_seconds() * ANT_FACING_ROTATION_SPEED / angle_between;
					transform.rotation = transform.rotation.slerp(rotation, interpolation);
				}
				else {transform.rotation = rotation;}
			}
		}
	}
}

pub fn ant_death_system
(
	mut commands: Commands,
	mut ants: Query
	<
		(
			Entity,
			&mut Sprite
		),
		(
			With<IsDead>,
			With<AntClass>,
		),
	>,
)
{
	for (ant, mut sprite) in ants.iter_mut()
	{
		commands
			.entity(ant)
			.remove::<AntClass>()
			.remove::<AntColonyAllegiance>()
			.remove::<ScoutClass>()
			.remove::<WorkerClass>()
			.remove::<SoldierClass>()
			.remove::<Velocity>()
			.insert(Damping
			{
				linear_damping: 10.0,
				angular_damping: 1.0,
				..default()
			});
		sprite.color = ANT_CORPSE_COLOR;
	}
}