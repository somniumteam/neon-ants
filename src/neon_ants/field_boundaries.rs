use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use bevy_rapier2d::prelude::*;
use rand::Rng;

pub struct FieldBoundariesPlugin;
impl Plugin for FieldBoundariesPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(PreUpdate, update_field_boundaries_system);
		app.add_systems(FixedUpdate,
		(
			enforce_field_boundaries_system.run_if(resource_exists::<FieldBoundaries>),
		));
	}
}

#[derive(Resource, Default)]
pub struct FieldBoundaries
{
	pub minimum: Vec2,
	pub maximum: Vec2,
}
impl FieldBoundaries
{
	pub fn size(self: &Self) -> Vec2
	{
		self.maximum - self.minimum
	}
	pub fn rand(self: &Self) -> Vec2
	{
		let mut rng = rand::thread_rng();
		Vec2
		{
			x: rng.gen_range(self.minimum.x..self.maximum.x),
			y: rng.gen_range(self.minimum.y..self.maximum.y),
		}
	}
}

pub fn update_field_boundaries_system
(
	mut commands: Commands,
	window: Query<&Window, With<PrimaryWindow>>,
	boundaries: Option<ResMut<FieldBoundaries>>,
)
{
	let window = window.single();
	let size = Vec2 {x: window.width(), y: window.height()};
	if size == Vec2::ZERO {return;}
	let minimum = size * -0.5;
	let maximum = minimum * -1.0;
	if let Some(mut boundaries) = boundaries
	{
		if minimum != boundaries.minimum {boundaries.minimum = minimum;}
		if maximum != boundaries.maximum {boundaries.maximum = maximum;}
	}
	else
	{
		commands.insert_resource(FieldBoundaries
		{
			minimum,
			maximum,
		});
	}
}

pub fn enforce_field_boundaries_system
(
	mut query: Query
	<(
		&mut Transform,
		&RigidBody,
	)>,
	field: Res<FieldBoundaries>,
)
{
	for (mut transform, rigid_body) in query.iter_mut()
	{
		if *rigid_body != RigidBody::Fixed
		{
			let pos = transform.translation;
			if pos.x < field.minimum.x {transform.translation.x = field.minimum.x;}
			else if pos.y < field.minimum.y {transform.translation.y = field.minimum.y;}
			if pos.x > field.maximum.x {transform.translation.x = field.maximum.x;}
			else if pos.y > field.maximum.y {transform.translation.y = field.maximum.y;}
		}
	}
}