use bevy::prelude::*;
use rand::Rng;
use std::fmt::Display;
use crate::*;

pub const ANT_COLONY_DEFAULT_RESOURCES: f32 = ANT_COST * 3.0;
pub const ANT_COLONY_WIDTH: f32 = 48.0;
pub const ANT_COLONY_REPORT_PRUNE_RADIUS: f32 = 32.0;
pub const ANT_COLONY_KINEMATIC_ANTS_RADIUS: f32 = (ANT_COLONY_WIDTH + ANT_WIDTH ) / 2.0;
pub const ANT_COST: f32 = 10.0;
pub const ANT_COLONY_SPAWN_COUNT: usize = 6;
pub const ANT_COLONY_HEALTH: f32 = 100.0;

pub struct AntColonyPlugin;
impl Plugin for AntColonyPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<AntColony>();
		app.register_type::<AntColonyStocks>();
		app.register_type::<AntColonyAllegiance>();
		app.add_systems(Startup, setup);
		app.add_systems(PreUpdate,
		(
			game_start_colony_spawning_system.run_if(resource_exists::<FieldBoundaries>),
		));
		app.add_systems(FixedUpdate,
		(
			colony_ant_spawn_system,
		));
	}
}
fn setup
(
	mut commands: Commands,
    asset_server: Res<AssetServer>,
)
{
	commands.insert_resource(AntColonySingletons
	{
		colony_image: asset_server.load("AntColony.png"),
	});
}

pub fn game_start_colony_spawning_system
(
	mut commands: Commands,
	mut restarts: EventReader<GameRestartEvent>,
	singletons: Res<AntColonySingletons>,
	boundaries: Res<FieldBoundaries>,
)
{
	if !restarts.is_empty()
	{
		restarts.clear();
		let mut rand = rand::thread_rng();
		for _ in 0..ANT_COLONY_SPAWN_COUNT
		{
			let color = Color::srgb_u8(rand.gen_range(0..=255), rand.gen_range(0..=255), rand.gen_range(0..=255));
			commands
				.spawn(AntColonyBundle::new
				(
					&singletons,
					color,
				))
				.insert(Transform::from_translation(boundaries.rand().extend(-1.0)));
		}
	}
}

#[derive(Component, Clone, Reflect)]
pub struct AntColonyAllegiance
{
	pub colony: Entity,
	pub color: Color,
}
pub fn hex_format_color(color: Color) -> String
{
	let color = color.to_srgba();
	let hex_raw = [color.red, color.green, color.blue];
	let mut hex: [u8; 3] = default();
	for i in 0..3 {hex[i] = (hex_raw[i] * 256.0) as u8;}
	format!("{:02X?}{:02X?}{:02X?}", hex[0], hex[1], hex[2])
}
impl Display for AntColonyAllegiance
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "{}", hex_format_color(self.color))
	}
}

#[derive(Component, Clone, Copy, Reflect)]
pub struct AntColonyStocks
{
	pub resources: f32,
}
impl Default for AntColonyStocks
{
	fn default() -> Self
	{
		Self
		{
			resources: ANT_COLONY_DEFAULT_RESOURCES,
		}
	}
}

#[derive(Default, Copy, Clone)]
pub struct ClassSums([usize; ANT_CLASS_COUNT]);
impl ClassSums
{
	pub fn total_ants(self: &Self) -> usize {self.0.iter().sum()}
	pub fn calculate_class_ratios(self: &Self) -> [f32; ANT_CLASS_COUNT]
	{
		let total = self.total_ants();
		let mut ret_val: [f32; ANT_CLASS_COUNT] = default();
		self.0.iter().map(|s| (*s as f32) / (total as f32)).enumerate().for_each(|(i, ratio)|
		{
			ret_val[i] = ratio;
		});
		ret_val
	}
	pub fn ant_class_spawn_candidate(self: &Self) -> AntClass
	{
		for (i, class) in ANT_CLASSES.iter().enumerate()
		{
			let minimum = ANT_CLASS_MINIMUMS[i];
			let current = self.0[i];
			if current < minimum {return *class;}
		}
		let ratios = self.calculate_class_ratios();
		let mut highest_ratio_diff: f32 = default();
		let mut highest_ratio_class: AntClass = default();
		for (i, class) in ANT_CLASSES.iter().enumerate()
		{
			let current_ratio = ratios[i];
			let target_ratio = ANT_CLASS_RATIOS[i];
			let ratio_diff = target_ratio - current_ratio;
			if ratio_diff > highest_ratio_diff
			{
				highest_ratio_diff = ratio_diff;
				highest_ratio_class = *class;
			}
		}
		highest_ratio_class
	}
}

#[derive(Component, Default, Reflect)]
pub struct AntColony
{
	pub color: Color,
}
#[derive(Bundle)]
pub struct AntColonyBundle
{
	pub name: Name,
	pub sprite: SpriteBundle,
	pub stocks: AntColonyStocks,
	pub colony: AntColony,
	pub reports: SavedReports,
	pub health: Health,
}
#[derive(Resource, Clone)]
pub struct AntColonySingletons
{
	pub colony_image: Handle<Image>,
}
impl AntColonyBundle
{
	pub fn new
	(
		singletons: &AntColonySingletons,
		color: Color,
	) -> Self
	{
		Self
		{
			name: Name::new(format!("Colony {}", hex_format_color(color))),
			sprite: SpriteBundle
			{
				texture: singletons.colony_image.clone(),
				sprite: Sprite
				{
					custom_size: Some(Vec2::splat(ANT_COLONY_WIDTH)),
					color,
					..default()
				},
				..default()
			},
			colony: AntColony {color, ..default()},
			stocks: default(),
			reports: default(),
			health: Health::new(ANT_COLONY_HEALTH),
		}
	}
}

pub fn colony_ant_spawn_system
(
	mut commands: Commands,
	mut colony_query: Query
	<(
		Entity,
		&AntColony,
		&mut AntColonyStocks,
		&Transform,
	)>,
	ants: Query
	<(
		&AntColonyAllegiance,
		&AntClass,
	)>,
	singletons: Res<AntSingletons>,
)
{
	for (entity, colony, mut stocks, transform) in colony_query.iter_mut()
	{
		if stocks.resources >= ANT_COST
		{
			let mut class_totals = ClassSums::default();
			for (allegiance, class) in ants.iter()
			{
				if allegiance.colony == entity
				{
					class_totals.0[*class as usize] += 1;
				}
			}
			stocks.resources -= ANT_COST;
			let class = class_totals.ant_class_spawn_candidate();
			let mut commands = commands.spawn(AntBundle::new
			(
				&singletons,
				AntColonyAllegiance
				{
					colony: entity,
					color: colony.color,
				},
				class,
			));
			let commands = commands.insert(transform.clone());
			match class
			{
				AntClass::SCOUT => {commands.insert(ScoutClass::default());},
				AntClass::WORKER => {commands.insert(WorkerClass::default());}
				AntClass::SOLDIER => {commands.insert(SoldierClass::default());}
			};
		}
	}
}