use bevy::prelude::*;
use bevy::sprite::*;
use crate::*;

pub const ANT_WORKER_CARRY_CAPACITY: f32 = ANT_COST / 4.0;
pub const ANT_WORKER_GATHER_SPEED: f32 = ANT_WORKER_CARRY_CAPACITY / 2.0;
pub const ANT_WORKER_CARRY_INDICATOR_SCALE: f32 = 0.7;
pub const ANT_WORKER_FIND_RADIUS: f32 = ANT_SCOUT_FIND_RADIUS;
pub const ANT_WORKER_REPORT_NONE_RADIUS: f32 = ANT_HEIGHT;
pub const ANT_WORKER_SPEED: f32 = ANT_BASE_SPEED;
pub const ANT_WORKER_BITE_RADIUS: f32 = 3.0;

pub struct AntWorkerPlugin;
impl Plugin for AntWorkerPlugin
{
	fn build(&self, app: &mut App)
	{
		app.register_type::<WorkerClass>();
		app.register_type::<WorkerHarvestTarget>();
		app.add_systems(Startup, setup);
		app.add_systems(FixedUpdate,
		(
			worker_receive_report_system,
			worker_gather_food_system,
			worker_grab_food_system,
			worker_dock_system,
		));
		app.add_systems(Update, worker_carry_indicator_system);
	}
}

fn setup
(
	mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
	mut materials: ResMut<Assets<ColorMaterial>>,
)
{
	commands.insert_resource(AntWorkerSingletons
	{
		worker_carry_indicator_mesh: meshes.add(Circle::new(ANT_WORKER_CARRY_INDICATOR_SCALE * ANT_WIDTH / 2.0)).into(),
		worker_carry_indicator_material: materials.add(ColorMaterial {color: Color::srgb(0.0, 0.5, 0.0), ..default()}),
		worker_bite_mesh: meshes.add(Circle::new(ANT_WORKER_BITE_RADIUS)).into(),
		worker_bite_material: materials.add(ColorMaterial {color: BACKGROUND_COLOR, ..default()}),
	});
}

#[derive(Component, Default, Reflect)]
pub struct WorkerClass
{
	pub carried_resources: f32,
}
#[derive(Resource)]
pub struct AntWorkerSingletons
{
	pub worker_carry_indicator_mesh: Mesh2dHandle,
	pub worker_carry_indicator_material: Handle<ColorMaterial>,
	pub worker_bite_mesh: Mesh2dHandle,
	pub worker_bite_material: Handle<ColorMaterial>,
}

#[derive(Component, Reflect)]
pub struct WorkerHarvestTarget(pub Vec2);

pub fn worker_receive_report_system
(
	mut commands: Commands,
	workers: Query
	<
		(
			Entity,
			&AntColonyAllegiance,
			&RigidBody,
		),
		(
			With<WorkerClass>,
			Without<News>,
			Without<WorkerHarvestTarget>,
		),
	>,
	mut reports: EventReader<NewsDistributeEvent>,
)
{
	for report in reports.read()
	{
		if report.news.news_type == NewsType::Food
		{
			for (entity, allegiance, rigid_body) in workers.iter()
			{
				if *rigid_body == RigidBody::KinematicVelocityBased && allegiance.colony == report.colony
				{
					commands.entity(entity).insert(WorkerHarvestTarget(report.news.location));
					break;
				}
			}
		}
	}
}

pub fn worker_gather_food_system
(
	mut commands: Commands,
	mut workers: Query
	<
		(
			Entity,
			&mut Velocity,
			&Transform,
			&WorkerHarvestTarget,
		),
		Without<News>,
	>,
	food: Query
	<
		&Transform,
		(
			With<IsPlant>,
			With<Health>,
		),
	>,
)
{
	for (entity, mut velocity, worker_transform, harvest) in workers.iter_mut()
	{
		let worker_xy = worker_transform.translation.xy();
		let mut target_pos = harvest.0;
		let nearest_food = food
			.iter()
			.map(|transform|
			(
				transform,
				transform.translation.xy().distance_squared(worker_xy),
			))
			.filter(|(_, dist2)| *dist2 <= ANT_WORKER_FIND_RADIUS * ANT_WORKER_FIND_RADIUS)
			.min_by(|(_, dist_a_2), (_, dist_b_2)| dist_a_2.total_cmp(&dist_b_2))
			.map(|(transform, _)| transform);
		if let Some(nearest_food_transform) = nearest_food
		{
			target_pos = nearest_food_transform.translation.xy();
		}
		let delta = target_pos - worker_xy;
		let dist2 = delta.length_squared();
		if dist2 <= ANT_WORKER_REPORT_NONE_RADIUS * ANT_WORKER_REPORT_NONE_RADIUS
		{
			commands.entity(entity)
				.insert(News
				{
					location: harvest.0,
					news_type: NewsType::Food,
					not_found: true,
				})
				.remove::<WorkerHarvestTarget>();
		}
		else {velocity.linvel = delta.normalize_or_zero() * ANT_WORKER_SPEED;}
	};
}

pub fn worker_dock_system
(
	mut workers: Query
	<
		(
			&mut WorkerClass,
			&mut Velocity,
			&mut Transform,
			&mut RigidBody,
			&AntColonyAllegiance,
		),
		(
			Without<WorkerHarvestTarget>,
			Without<News>,
			Without<AntColonyStocks>,
		),
	>,
	mut colonies: Query
	<(
		&mut AntColonyStocks,
		&Transform,
	)>,
)
{
	for
	(
		mut worker,
		mut velocity,
		mut transform,
		mut rigid_body,
		allegiance,
	) in workers.iter_mut()
	{
		if let Ok((mut stocks, colony_transform)) = colonies.get_mut(allegiance.colony)
		{
			let delta = colony_transform.translation.xy() - transform.translation.xy();
			let dist2 = delta.length_squared();
			let radius = ANT_COLONY_WIDTH / 2.0;
			if dist2 <= radius * radius
			{
				if worker.carried_resources > 0.0
				{
					stocks.resources += worker.carried_resources;
					worker.carried_resources = 0.0;
				}
				if dist2 != 0.0
				{
					transform.translation = colony_transform.translation;
					velocity.linvel = default();
					*rigid_body = RigidBody::KinematicVelocityBased;
				}
			}
			else {velocity.linvel = delta.normalize_or_zero() * ANT_WORKER_SPEED;}
		}
	}
}

#[derive(Component, Default)]
pub struct IsWorkerCarryIndicator;
#[derive(Bundle, Default)]
pub struct WorkerCarryIndicatorBundle
{
	pub name: Name,
	pub mesh: MaterialMesh2dBundle<ColorMaterial>,
	pub is_worker_carry_indicator: IsWorkerCarryIndicator,
}
impl WorkerCarryIndicatorBundle
{
	pub fn new(singletons: &AntWorkerSingletons) -> Self
	{
		Self
		{
			name: Name::new("Worker Carry Indicator"),
			mesh: MaterialMesh2dBundle
			{
				mesh: singletons.worker_carry_indicator_mesh.clone(),
				material: singletons.worker_carry_indicator_material.clone(),
				..default()
			},
			..default()
		}
	}
}

pub fn worker_carry_indicator_system
(
	mut commands: Commands,
	workers: Query
	<(
		Entity,
		&WorkerClass,
		Option<&Children>,
	)>,
	mut indicators: Query
	<
		&mut Transform,
		With<IsWorkerCarryIndicator>,
	>,
	singletons: Res<AntWorkerSingletons>,
)
{
	for (worker, class, children) in workers.iter()
	{
		let mut found_child = false;
		if class.carried_resources > 0.0
		{
			let scale = Vec3::splat(class.carried_resources / ANT_WORKER_CARRY_CAPACITY);
			if let Some(children) = children
			{
				for child in children
				{
					if let Ok(mut transform) = indicators.get_mut(*child)
					{
						found_child = true;
						transform.scale = scale;
						break;
					}
				}
			}
			if !found_child
			{
				commands.entity(worker).with_children(|builder|
				{
					builder
						.spawn(WorkerCarryIndicatorBundle::new(&singletons))
						.insert(Transform {scale, translation: Vec3::new(0.0, 0.0, 0.1), ..default()});
				});
			}
		}
		else if let Some(children) = children
		{
			for child in children
			{
				if indicators.get(*child).is_ok()
				{
					commands.entity(*child).despawn_recursive();
					break;
				}
			}
		}
	}
}

pub fn worker_grab_food_system
(
	mut commands: Commands,
	mut workers: Query<(Entity, &mut WorkerClass)>,
	mut food: Query<(Entity, &mut Health, &Transform), With<IsPlant>>,
	rapier_context: Res<RapierContext>,
	time: Res<Time>,
	singletons: Res<AntWorkerSingletons>,
)
{
	for (worker_entity, mut worker) in workers.iter_mut()
	{
		for (food_entity, mut food_health, food_transform) in food.iter_mut()
		{
			if let Some(pair) = rapier_context.contact_pair(worker_entity, food_entity)
			{
				if pair.has_any_active_contact()
				{
					let taken = food_health.current
						.min(ANT_WORKER_CARRY_CAPACITY - worker.carried_resources)
						.min(ANT_WORKER_GATHER_SPEED * time.delta_seconds());
					if taken > 0.0
					{
						worker.carried_resources += taken;
						food_health.current -= taken;
						let first_collider = pair.collider1() == food_entity;
						let contact = pair.manifold(0).unwrap();
						let contact = contact.point(0).unwrap();
						let contact = if first_collider {contact.local_p1()} else {contact.local_p2()};
						commands.entity(food_entity).with_children(|builder|
						{
							builder.spawn(worker_bite_bundle(contact, &singletons));
						});
					}
					if worker.carried_resources >= ANT_WORKER_CARRY_CAPACITY
					{
						commands.entity(worker_entity)
							.insert(News
							{
								location: food_transform.translation.xy(),
								news_type: NewsType::Food,
								not_found: false,
							})
							.remove::<WorkerHarvestTarget>();
					}
				}
			}
		}
	}
}

pub fn worker_bite_bundle(position: Vec2, singletons: &AntWorkerSingletons) -> TempEffectBundle<MaterialMesh2dBundle<ColorMaterial>>
{
	TempEffectBundle::new
	(
		"Worker Bite",
		MaterialMesh2dBundle::<ColorMaterial>
		{
			mesh: singletons.worker_bite_mesh.clone(),
			material: singletons.worker_bite_material.clone(),
			transform: Transform::from_translation(position.extend(1.0)),
			..default()
		},
		None,
	)
}