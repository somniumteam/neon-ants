use bevy::prelude::*;
use bevy::sprite::*;
use bevy_rapier2d::prelude::*;
use super::super::*;

pub const SOYLENT_GREEN_WIDTH: f32 = 16.0;
pub const SOYLENT_GREEN_HEALTH: f32 = 100.0;
pub const MINIMUM_PLANTS_COUNT: usize = 12;

pub struct PlantsPlugin;
impl Plugin for PlantsPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_systems(Startup, setup);
		app.add_systems(FixedUpdate,
		(
			minimum_plants_system,
		));
	}
}
fn setup
(
	mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
)
{
	commands.insert_resource(SoylentGreenSingletons
	{
		mesh: meshes.add(Circle::new(SOYLENT_GREEN_WIDTH / 2.0)).into(),
		material: materials.add(ColorMaterial{color: Color::srgb(0.0, 0.4, 0.0), ..default()}),
	});
}

#[derive(Component)]
pub struct IsPlant;

#[derive(Bundle)]
pub struct SoylentGreenBundle
{
	pub name: Name,
	pub mesh: MaterialMesh2dBundle<ColorMaterial>,
	pub health: Health,
	pub collider: Collider,
	pub rigid_body: RigidBody,
	pub velocity: Velocity,
	pub is_plant: IsPlant,
	pub damping: Damping,
	pub remove_on_death: RemoveOnDeath,
}
impl Default for SoylentGreenBundle
{
	fn default() -> Self
	{
		Self
		{
			name: Name::new("Soylent Green"),
			mesh: MaterialMesh2dBundle::<ColorMaterial>::default(),
			health: Health::new(SOYLENT_GREEN_HEALTH),
			collider: Collider::ball(SOYLENT_GREEN_WIDTH / 2.0),
			rigid_body: RigidBody::Dynamic,
			velocity: default(),
			is_plant: IsPlant,
			damping: Damping
			{
				linear_damping: 5.0,
				..default()
			},
			remove_on_death: RemoveOnDeath,
		}
	}
}
#[derive(Resource, Clone)]
pub struct SoylentGreenSingletons
{
	pub mesh: Mesh2dHandle,
	pub material: Handle<ColorMaterial>,
}
impl SoylentGreenBundle
{
	pub fn new(singletons: &SoylentGreenSingletons) -> Self
	{
		Self
		{
			mesh: MaterialMesh2dBundle
			{
				mesh: singletons.mesh.clone(),
				material: singletons.material.clone(),
				..default()
			},
			..default()
		}
	}
}

pub fn minimum_plants_system
(
	mut commands: Commands,
	plants: Query<(), With<IsPlant>>,
	field: Res<FieldBoundaries>,
	soylent: Res<SoylentGreenSingletons>,
)
{
	if plants.iter().len() == 0
	{
		for _ in 0..MINIMUM_PLANTS_COUNT
		{
			commands
				.spawn(SoylentGreenBundle::new(&soylent))
				.insert(Transform
				{
					translation: field.rand().extend(0.0),
					..default()
				});
		}
	}
}