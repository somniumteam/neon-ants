use bevy::prelude::*;
use bevy::window::PrimaryWindow;
use bevy_egui::*;
use bevy_inspector_egui::*;

pub struct DebugGUIPlugin;
impl Plugin for DebugGUIPlugin
{
	fn build(&self, app: &mut App)
	{
		app.add_plugins
		((
			EguiPlugin,
			bevy_inspector_egui::DefaultInspectorConfigPlugin,
		))
			.add_systems(Update, toggle_debug_gui)
			.add_systems(Last, update_debug_gui)
			.init_resource::<DebugGuiOpen>();
	}
}

#[derive(Default, Resource, Debug, Copy, Clone)]
struct DebugGuiOpen(bool);

fn toggle_debug_gui(mut gui_open: ResMut<DebugGuiOpen>, input: Res<ButtonInput<KeyCode>>)
{
	if input.just_pressed(KeyCode::Backquote)
	{
		let gui_open = gui_open.as_mut();
		gui_open.0 = !gui_open.0;
	}
}

fn update_debug_gui(world: &mut World)
{
	let gui_open = world.resource::<DebugGuiOpen>().0;
	if !gui_open
	{
		world.resource_mut::<Time<Virtual>>().unpause();
		return;
	}
	world.resource_mut::<Time<Virtual>>().pause();

	let Ok(egui_context) = world
		.query_filtered::<&mut EguiContext, With<PrimaryWindow>>()
		.get_single(world)
	else {return;};

	let mut egui_context = egui_context.clone();
	let context = egui_context.get_mut();

	egui::Window::new("DEBUG_GUI").show(&context, |ui|
	{
		egui::ScrollArea::vertical().show(ui, |ui|
		{
			bevy_inspector::ui_for_world(world, ui);
		});
	});
}